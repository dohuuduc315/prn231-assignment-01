﻿namespace DataAccess.DAO
{
    public class ProductDAO
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public decimal UnitPrice { get; set; }
        public string UnitsInStock { get; set; }
        public int CategoryId { get; set; }
    }
}
