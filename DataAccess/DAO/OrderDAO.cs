﻿namespace DataAccess.DAO
{
    public class OrderDAO
    {
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime ShippedDate { get; set; }
        public string Freight { get; set; }
        public int MemberId { get; set; }
    }
}
