﻿using BusinessObject;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.Services;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class GenericRepo<T> : IGenericRepo<T> where T : BaseEntity
    {
        private readonly AppDbContext _context;
        protected DbSet<T> _dbSet;
        private readonly ICurrentTimeService _currentTimeService;

        public GenericRepo(AppDbContext context, ICurrentTimeService currentTimeService)
        {
            _context = context;
            _dbSet = _context.Set<T>();
            _currentTimeService = currentTimeService;
        }

        public async Task CreateAsync(T entity)
        {
            //entity.CreationDate = _currentTimeService.GetCurrentTime();
            await _dbSet.AddAsync(entity);
        }

        public void DeleteAsync(T entity)
        {
            //entity.DeletionDate = _currentTimeService.GetCurrentTime();
            entity.IsDeleted = true;
            _dbSet.Update(entity);
        }

        public async Task<IEnumerable<T>> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();

        public async Task<T> GetEntityByIdAsync(int id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }

        public void UpdateAsync(T entity)
        {
            //entity.ModificationDate = _currentTimeService.GetCurrentTime();
            _dbSet.Update(entity);
        }
    }
}
