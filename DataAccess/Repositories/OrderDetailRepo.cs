﻿using BusinessObject;
using BusinessObject.Entities;
using DataAccess.Services;

namespace DataAccess.Repositories
{
    public class OrderDetailRepo : GenericRepo<OrderDetail>, IOrderDetailRepo
    {
        public OrderDetailRepo(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }
    }
}
