﻿using BusinessObject;
using BusinessObject.Entities;
using DataAccess.Services;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class MemberRepo : GenericRepo<Member>, IMemberRepo
    {
        public MemberRepo(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<bool> ExistEmail(string email) => await _dbSet.AnyAsync(x => x.Email == email);

        public Task<Member?> Login(string email, string password)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Email == email && x.Password == password);
        }
    }
}
