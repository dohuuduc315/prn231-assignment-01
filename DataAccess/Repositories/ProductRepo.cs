﻿using BusinessObject;
using BusinessObject.Entities;
using DataAccess.Services;

namespace DataAccess.Repositories
{
    public class ProductRepo : GenericRepo<Product>, IProductRepo
    {
        public ProductRepo(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }
    }
}
