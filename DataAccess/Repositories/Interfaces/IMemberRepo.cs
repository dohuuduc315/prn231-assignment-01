﻿using BusinessObject.Entities;

namespace DataAccess.Repositories
{
    public interface IMemberRepo : IGenericRepo<Member>
    {
        Task<Member?> Login(string email, string password);
        Task<bool> ExistEmail(string email);
    }
}
