﻿using BusinessObject.Entities;
using DataAccess.Commons;

namespace DataAccess.Repositories
{
    public interface IGenericRepo<T> where T : BaseEntity
    {
        Task CreateAsync(T entity);
        void UpdateAsync(T entity);
        void DeleteAsync(T entity);
        Task<T> GetEntityByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10);
    }
}
