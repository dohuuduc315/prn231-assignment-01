﻿using BusinessObject.Entities;

namespace DataAccess.Repositories
{
    public interface IOrderRepo : IGenericRepo<Order>
    {
    }
}
