﻿using BusinessObject.Entities;

namespace DataAccess.Repositories
{
    public interface IProductRepo : IGenericRepo<Product>
    {
    }
}
