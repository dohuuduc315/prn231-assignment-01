﻿using BusinessObject.Entities;

namespace DataAccess.Repositories
{
    public interface IOrderDetailRepo : IGenericRepo<OrderDetail>
    {
    }
}
