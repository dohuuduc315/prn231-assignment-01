﻿using BusinessObject;
using BusinessObject.Entities;
using DataAccess.Services;

namespace DataAccess.Repositories
{
    public class OrderRepo : GenericRepo<Order>, IOrderRepo
    {
        public OrderRepo(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }
    }
}
