﻿using AutoMapper;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Product, ProductDAO>().ReverseMap();
            CreateMap<Order, OrderDAO>().ReverseMap();
            CreateMap<OrderDetail, OrderDetailDAO>().ReverseMap();
            CreateMap<Category, CategoryDAO>().ReverseMap();
            CreateMap<Member, MemberDAO>().ReverseMap();
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
        }
    }
}
