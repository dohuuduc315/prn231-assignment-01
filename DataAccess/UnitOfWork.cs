﻿using BusinessObject;
using DataAccess.Repositories;
using DataAccess.Services;

namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly ICurrentTimeService _currentTimeService;

        public UnitOfWork(AppDbContext context, ICurrentTimeService currentTimeService)
        {
            _context = context;
            _currentTimeService = currentTimeService;
        }
        public IMemberRepo MemberRepo => new MemberRepo(_context, _currentTimeService);

        public IProductRepo ProductRepo => new ProductRepo(_context, _currentTimeService);

        public IOrderDetailRepo OrderDetailRepo => new OrderDetailRepo(_context, _currentTimeService);

        public IOrderRepo OrderRepo => new OrderRepo(_context, _currentTimeService);

        public Task<int> SaveChangeAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
