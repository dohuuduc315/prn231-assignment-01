﻿using DataAccess.Repositories;

namespace DataAccess
{
    public interface IUnitOfWork
    {
        public Task<int> SaveChangeAsync();
        public IMemberRepo MemberRepo { get; }
        public IProductRepo ProductRepo { get; }
        public IOrderDetailRepo OrderDetailRepo { get; }
        public IOrderRepo OrderRepo { get; }
    }
}
