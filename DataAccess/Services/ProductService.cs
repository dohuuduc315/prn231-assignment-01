﻿using AutoMapper;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddProduct(ProductDAO productDAO)
        {
            var productObj = _mapper.Map<Product>(productDAO);
            await _unitOfWork.ProductRepo.CreateAsync(productObj);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteProduct(int id)
        {
            var productObj = await _unitOfWork.ProductRepo.GetEntityByIdAsync(id);
            if (productObj is not null)
            {
                _unitOfWork.ProductRepo.DeleteAsync(productObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<Product>> GetAllProductsAsync(int pageIndex = 1, int pageSize = 10)
        {
            return await _unitOfWork.ProductRepo.ToPagination(pageIndex, pageSize);
        }

        public async Task<Product> GetProduct(int id)
        {
            return await _unitOfWork.ProductRepo.GetEntityByIdAsync(id);
        }

        public async Task UpdateProduct(int id, ProductDAO productDAO)
        {
            var productObj = await _unitOfWork.ProductRepo.GetEntityByIdAsync(id);
            if (productObj is not null)
            {
                _mapper.Map(productDAO, productObj);
                _unitOfWork.ProductRepo.UpdateAsync(productObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }
    }
}
