﻿using AutoMapper;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddOrderDetailAsync(OrderDetailDAO orderDetailDAO)
        {
            var orderDetailObj = _mapper.Map<OrderDetail>(orderDetailDAO);
            await _unitOfWork.OrderDetailRepo.CreateAsync(orderDetailObj);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteOrderDetailAsync(int id)
        {
            var orderDetailObj = await _unitOfWork.OrderDetailRepo.GetEntityByIdAsync(id);
            if (orderDetailObj is not null)
            {
                _unitOfWork.OrderDetailRepo.DeleteAsync(orderDetailObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<OrderDetail>> GetAllOrderDetailAsync(int pageIndex = 1, int pageSize = 10)
        {
            return await _unitOfWork.OrderDetailRepo.ToPagination(pageIndex, pageSize);
        }

        public async Task<OrderDetail> GetOrderDetailAsync(int id)
        {
            return await _unitOfWork.OrderDetailRepo.GetEntityByIdAsync(id);
        }

        public async Task UpdateOrderDetailAsync(int id, OrderDetailDAO orderDetailDAO)
        {
            var orderDetailObj = await _unitOfWork.OrderDetailRepo.GetEntityByIdAsync(id);
            if (orderDetailObj is not null)
            {
                _mapper.Map(orderDetailDAO, orderDetailObj);
                _unitOfWork.OrderDetailRepo.UpdateAsync(orderDetailObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }
    }
}
