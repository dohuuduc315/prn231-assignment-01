﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public interface IProductService
    {
        Task<Pagination<Product>> GetAllProductsAsync(int pageIndex = 1, int pageSize = 10);
        Task<Product> GetProduct(int id);
        Task AddProduct(ProductDAO productDAO);
        Task DeleteProduct(int id);
        Task UpdateProduct(int id, ProductDAO productDAO);
    }
}
