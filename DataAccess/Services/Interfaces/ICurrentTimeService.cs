﻿namespace DataAccess.Services
{
    public interface ICurrentTimeService
    {
        DateTime GetCurrentTime();
    }
}
