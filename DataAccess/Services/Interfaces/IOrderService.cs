﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public interface IOrderService
    {
        Task<Pagination<Order>> GetAllOrderAsync(int pageIndex = 1, int pageSize = 10);
        Task<Order> GetOrderAsync(int id);
        Task AddOrderAsync(OrderDAO orderDAO);
        Task DeleteOrderAsync(int id);
        Task UpdateOrderAsync(int id, OrderDAO orderDAO);
    }
}
