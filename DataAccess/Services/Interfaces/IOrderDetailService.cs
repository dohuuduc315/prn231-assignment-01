﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public interface IOrderDetailService
    {
        Task<Pagination<OrderDetail>> GetAllOrderDetailAsync(int pageIndex = 1, int pageSize = 10);
        Task<OrderDetail> GetOrderDetailAsync(int id);
        Task AddOrderDetailAsync(OrderDetailDAO orderDetailDAO);
        Task DeleteOrderDetailAsync(int id);
        Task UpdateOrderDetailAsync(int id, OrderDetailDAO orderDetailDAO);
    }
}
