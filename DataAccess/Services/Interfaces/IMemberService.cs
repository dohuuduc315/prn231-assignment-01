﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public interface IMemberService
    {
        Task<Member?> Login(string email, string password);
        Task<Pagination<Member>> GetAllMember(int pageIndex = 1, int pageSize = 10);
        Task<Member> GetMember(int id);
        Task AddMember(MemberDAO memberDAO);
        Task DeleteMember(int id);
        Task UpdateMember(int id, MemberDAO memberDAO);
    }
}
