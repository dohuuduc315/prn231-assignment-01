﻿using AutoMapper;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddOrderAsync(OrderDAO orderDAO)
        {
            var orderObj = _mapper.Map<Order>(orderDAO);
            await _unitOfWork.OrderRepo.CreateAsync(orderObj);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteOrderAsync(int id)
        {
            var orderObj = await _unitOfWork.OrderRepo.GetEntityByIdAsync(id);
            if (orderObj is not null)
            {
                _unitOfWork.OrderRepo.DeleteAsync(orderObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<Order>> GetAllOrderAsync(int pageIndex = 1, int pageSize = 10)
        {
            return await _unitOfWork.OrderRepo.ToPagination(pageIndex, pageSize);
        }

        public async Task<Order> GetOrderAsync(int id)
        {
            return await _unitOfWork.OrderRepo.GetEntityByIdAsync(id);
        }

        public async Task UpdateOrderAsync(int id, OrderDAO orderDAO)
        {
            var orderObj = await _unitOfWork.OrderRepo.GetEntityByIdAsync(id);
            if (orderObj is not null)
            {
                _mapper.Map(orderDAO, orderObj);
                _unitOfWork.OrderRepo.UpdateAsync(orderObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }
    }
}
