﻿using AutoMapper;
using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;

namespace DataAccess.Services
{
    public class MemberService : IMemberService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MemberService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddMember(MemberDAO memberDAO)
        {
            var isExist = await _unitOfWork.MemberRepo.ExistEmail(memberDAO.Email);

            if (!isExist)
            {
                var memberObj = _mapper.Map<Member>(memberDAO);
                await _unitOfWork.MemberRepo.CreateAsync(memberObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task DeleteMember(int id)
        {
            var memberObj = await _unitOfWork.MemberRepo.GetEntityByIdAsync(id);
            if (memberObj is not null)
            {
                _unitOfWork.MemberRepo.DeleteAsync(memberObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<Pagination<Member>> GetAllMember(int pageIndex = 1, int pageSize = 10)
        {
            return await _unitOfWork.MemberRepo.ToPagination(pageIndex, pageSize);
        }

        public async Task<Member> GetMember(int id)
        {
            return await _unitOfWork.MemberRepo.GetEntityByIdAsync(id);
        }

        public async Task<Member?> Login(string email, string password)
        {
            return await _unitOfWork.MemberRepo.Login(email, password);
        }

        public async Task UpdateMember(int id, MemberDAO memberDAO)
        {
            var memberObj = await _unitOfWork.MemberRepo.GetEntityByIdAsync(id);
            if (memberObj is not null)
            {
                _mapper.Map(memberDAO, memberObj);
                _unitOfWork.MemberRepo.UpdateAsync(memberObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }
    }
}
