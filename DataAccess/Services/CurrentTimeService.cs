﻿namespace DataAccess.Services
{
    public class CurrentTimeService : ICurrentTimeService
    {
        public DateTime GetCurrentTime() => DateTime.UtcNow;
    }
}
