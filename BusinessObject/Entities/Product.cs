﻿namespace BusinessObject.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public decimal UnitPrice { get; set; }
        public string UnitsInStock { get; set; }
        public int CategoryId { get; set; }
        public Category? Category { get; set; }
        public ICollection<OrderDetail>? OrderDetails { get; set; }
    }
}
