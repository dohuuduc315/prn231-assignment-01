﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessObject.Entities
{
    public class Member : BaseEntity
    {
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string City { get; set;}
        public string Country { get; set;}
        public string Password { get; set;}
        public ICollection<Order>? Orders { get; set; }
        [NotMapped] public bool isAdmin { get; set; }
    }
}
