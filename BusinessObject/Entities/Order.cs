﻿namespace BusinessObject.Entities
{
    public class Order : BaseEntity
    {
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime ShippedDate { get; set; }
        public string Freight { get; set; }
        public ICollection<OrderDetail>? OrderDetails { get; set; }
        public int MemberId { get; set; }
        public Member? Member { get; set; }
    }
}
