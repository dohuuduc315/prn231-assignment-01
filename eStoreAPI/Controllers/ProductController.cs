﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;
using DataAccess.Services;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<Pagination<Product>> Get(int pageIndex = 1, int pageSize = 10)
        {
            return await _productService.GetAllProductsAsync(pageIndex, pageSize);
        }

        [HttpGet("{id}")]
        public async Task<Product> GetById(int id)
        {
            return await _productService.GetProduct(id);
        }

        [HttpPost]
        public async Task Create(ProductDAO product) => await _productService.AddProduct(product);

        [HttpDelete("{id}")]
        public async Task Delete(int id) => await _productService.DeleteProduct(id);

        [HttpPut("{id}")]
        public async Task Update(int id, ProductDAO product) => await _productService.UpdateProduct(id, product);
    }
}
