﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;
using DataAccess.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IMemberService _memberService;
        private readonly IConfiguration _configuration;

        public MemberController(IMemberService memberService, IConfiguration configuration)
        {
            _memberService = memberService;
            _configuration = configuration;
        }

        public class LoginInformation
        {
            public string email { get; set; }
            public string password { get; set; }
        }

        [HttpPost("authentication")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult<Member?>> Login(LoginInformation loginInformation)
        {
            var member = await _memberService.Login(loginInformation.email, loginInformation.password);
            if (member is null)
            {
                if (loginInformation.email.Equals(_configuration["Admin:Email"], StringComparison.OrdinalIgnoreCase) && loginInformation.password.Equals(_configuration["Admin:Password"]))
                {
                    return new Member { Email = _configuration["Admin:Email"], Password = _configuration["Admin:Password"], isAdmin = true };
                }

                return NotFound();
            }

            return Ok(member);
        }

        [HttpGet]
        public async Task<Pagination<Member>> Get(int pageIndex = 1, int pageSize = 10)
        {
            return await _memberService.GetAllMember(pageIndex, pageSize);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _memberService.GetMember(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task Create(MemberDAO member) => await _memberService.AddMember(member);

        [HttpDelete("{id}")]
        public async Task Delete(int id) => await _memberService.DeleteMember(id);

        [HttpPut("{id}")]
        public async Task Update(int id, MemberDAO member) => await _memberService.UpdateMember(id, member);
    }
}
