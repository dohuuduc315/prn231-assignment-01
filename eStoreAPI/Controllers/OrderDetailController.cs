﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;
using DataAccess.Services;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailController : ControllerBase
    {
        private readonly IOrderDetailService _orderDetailService;

        public OrderDetailController(IOrderDetailService orderDetailService)
        {
            _orderDetailService = orderDetailService;
        }

        [HttpGet]
        public async Task<Pagination<OrderDetail>> Get(int pageIndex = 1, int pageSize = 10)
        {
            return await _orderDetailService.GetAllOrderDetailAsync(pageIndex, pageSize);
        }

        [HttpGet("{id}")]
        public async Task<OrderDetail> GetById(int id)
        {
            return await _orderDetailService.GetOrderDetailAsync(id);
        }

        [HttpPost]
        public async Task Create(OrderDetailDAO orderDetail) => await _orderDetailService.AddOrderDetailAsync(orderDetail);

        [HttpDelete("{id}")]
        public async Task Delete(int id) => await _orderDetailService.DeleteOrderDetailAsync(id);

        [HttpPut("{id}")]
        public async Task Update(int id, OrderDetailDAO orderDetail) => await _orderDetailService.UpdateOrderDetailAsync(id, orderDetail);
    }
}
