﻿using BusinessObject.Entities;
using DataAccess.Commons;
using DataAccess.DAO;
using DataAccess.Services;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<Pagination<Order>> Get(int pageIndex = 1, int pageSize = 10)
        {
            return await _orderService.GetAllOrderAsync(pageIndex, pageSize);
        }

        [HttpGet("{id}")]
        public async Task<Order> GetById(int id)
        {
            return await _orderService.GetOrderAsync(id);
        }

        [HttpPost]
        public async Task Create(OrderDAO order) => await _orderService.AddOrderAsync(order);

        [HttpDelete("{id}")]
        public async Task Delete(int id) => await _orderService.DeleteOrderAsync(id);

        [HttpPut("{id}")]
        public async Task Update(int id, OrderDAO order) => await _orderService.UpdateOrderAsync(id, order);
    }
}
