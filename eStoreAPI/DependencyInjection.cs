﻿using BusinessObject;
using Microsoft.EntityFrameworkCore;

namespace eStoreAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection BuildServices(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("ConnectionStrings").Get<ConnectionStrings>();
            services.AddSingleton(connectionString);
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectionString.SQLServerDB));
            var jwtSection = configuration.GetSection("JWTSection").Get<JWTSection>();
            services.AddSingleton(jwtSection);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            return services;
        }
    }
}
